"use strict"

const csrfToken = document.getElementById("csrfToken").value;
const validateRoute = document.getElementById("validateRoute").value;
const blogRoute = document.getElementById("getRoute").value;
const addRoute = document.getElementById("addRoute").value;
const createRoute = document.getElementById("createRoute").value;
const logoutRoute = document.getElementById("logoutRoute").value;

function login() {
    const username = document.getElementById("loginName").value;
    const password = document.getElementById("loginPass").value;
    const errorMessage = document.getElementById("login-fail");
    
    if (username == "" || password == ""){
        errorMessage.className="alert alert-danger"
            errorMessage.role = "alert"
            errorMessage.textContent = "Username or Password is incorrect. Login Failed"
    } else if (is_email(username) == false){
        errorMessage.className="alert alert-danger"
            errorMessage.role = "alert"
            errorMessage.textContent = "Username or Password is incorrect. Login Failed"
    } else {
    fetch(validateRoute, {
        method: 'POST',
        headers: {'Content-Type': 'application/json', 'Csrf-Token': csrfToken},
        body: JSON.stringify({username, password})
    }).then(res => res.json()).then(data =>{
        console.log(data)
        if(data) {
            switchToPost()
            loadBlog()
        } else {
            errorMessage.className="alert alert-danger"
            errorMessage.role = "alert"
            errorMessage.textContent = "Username or Password is incorrect. Login Failed"
        }
    });
    }

}

function createUser() {
    const username = document.getElementById("newName").value;
    const password = document.getElementById("newPass").value;
    const signup_fail = document.getElementById("signup-fail");
    if (username == "" || password == ""){               
        signup_fail.className="alert alert-danger"
        signup_fail.role = "alert"
        signup_fail.textContent = "Please enter valid email and password. Create User Failed"                                 

    } else if (is_email(username) == false){
        signup_fail.className="alert alert-danger"
        signup_fail.role = "alert"
        signup_fail.textContent = "Please enter valid email. Create User Failed"
    } else {
        fetch(createRoute, {
            method: 'POST',
            headers: {'Content-Type': 'application/json', 'Csrf-Token': csrfToken},
            body: JSON.stringify({username, password})
        }).then(res => res.json()).then(data =>{
            console.log(data)
            if(data) {
                switchToPost()
                removeLoginAlert()
                loadBlog()
            } else {
                signup_fail.className="alert alert-danger"
                signup_fail.role = "alert"
                signup_fail.textContent = "Username existed. Create User Failed"
            }
        });
    }
}


function loadBlog() {
    const ul = document.getElementById("posts")
    ul.innerHTML = ""
    fetch(blogRoute).then(res => res.json()).then(blogs => {
        switchToPost()

        for (const blog of blogs) {
            const newCard = document.createElement("div")
            newCard.className = "card mb-4"

            const newPicture = document.createElement("img")
            newPicture.className = "card-img-top"
            newPicture.src = "https://blog.thesocialms.com/wp-content/uploads/2016/07/12-Things-To-Do-After-You-Have-Written-A-New-Blog-Post-750x346.jpg"
            newPicture.style = "height=300px"
            newPicture.alt = "New Blog Image"

            const newCardBody = document.createElement("div")
            newCardBody.className = "card-body"

            const postName = document.createElement("h2")
            postName.className="card-title"
            postName.textContent = blog[0]

            const postOwner = document.createElement("div")
            postOwner.className = "card-footer text-muted"
            postOwner.textContent = "Posted By " + blog[1]

            const postContent = document.createElement("p")
            postContent.className="card-text"
            postContent.textContent = blog[2]


            newCardBody.appendChild(postName)
            newCardBody.appendChild(postContent)

            newCard.appendChild(newPicture)
            newCard.appendChild(newCardBody)
            newCard.appendChild(postOwner)

            ul.appendChild(newCard)


        
        }
    })
}

function addPost() {
    const post = document.getElementById("newPost").value;
    const title = document.getElementById("post-title").value;
    if (post == "" || title == "") {
        document.getElementById("post-message").innerHTML = "Input your post title and body. Failed to add post"
    } else {
        console.log(title)
        fetch(addRoute, {
            method: 'POST',
            headers: {'Content-Type': 'application/json', 'Csrf-Token': csrfToken },
            body: JSON.stringify({title, post})
        }).then(res => res.json()).then(data => {
            console.log(data)
            if (data) {
                loadBlog()
                document.getElementById("newPost").value = ""
                document.getElementById("post-message").innerHTML = ""
                document.getElementById("post-title").value = ""
            } else {
                document.getElementById("post-message").innerHTML = "Failed to add post"
            }
        })
    }
}

function logout() {
    fetch(logoutRoute).then(res => res.json()).then(blog => {
        switchToLogin()
        clearInput()
        removeLoginAlert()
    });
}

function is_email(email){
    var emailReg = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    return emailReg.test(email);
}

function removeAlert(section) {
    section.className = ""
    section.textContent = ""
}

function clearInput() {
    document.getElementById("newName").value = ""
    document.getElementById("newPass").value = ""
    document.getElementById("loginName").value = ""
    document.getElementById("loginPass").value = ""
}

function switchToPost() {
    document.getElementById("login-section").hidden = true
    document.getElementById("blog-section").hidden = false
    document.getElementById("login-header").hidden = true
    document.getElementById("blog-header").hidden = false
}

function switchToLogin() {
    document.getElementById("login-section").hidden = false
    document.getElementById("blog-section").hidden = true
    document.getElementById("add-section").hidden = true
    document.getElementById("blog-header").hidden = true
    document.getElementById("login-header").hidden = false
}

function removeLoginAlert() {
    removeAlert(document.getElementById("login-fail"))
    removeAlert(document.getElementById("signup-fail"))
}


function switchToAdd(){
    document.getElementById("login-section").hidden = true
    document.getElementById("blog-section").hidden = true
    document.getElementById("add-section").hidden = false
}