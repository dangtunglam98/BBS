package controllers

import javax.inject._
import play.api._
import play.api.mvc._
import play.api.libs.json.{JsError, JsSuccess, Json}
import models._
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.{ExecutionContext, Future}

/**
 * This controller creates an `Action.async` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class BlogController @Inject()(protected val dbConfigProvider: DatabaseConfigProvider, controllerComponents: ControllerComponents)
                              (implicit ec: ExecutionContext)
  extends AbstractController(controllerComponents) with HasDatabaseConfigProvider[JdbcProfile] {

    private val model = new BlogPostInDatabase(db)
    def load = Action.async {implicit requests =>
        Future.successful(Ok(views.html.BlogViews()));
    }

    implicit val postDataReads = Json.reads[PostData]

    implicit val userDataReads = Json.reads[UserData]
    def validate = Action.async { implicit requests =>
        requests.body.asJson.map { body =>
            Json.fromJson[UserData](body) match {
                case JsSuccess(ud, path) =>
                    model.validateUser(ud.username, ud.password).map { userExist =>
                        if (userExist) {
                            Ok(Json.toJson(true))
                              .withSession("username" -> ud.username, "csrfToken" -> play.filters.csrf.CSRF.getToken.map(_.value).getOrElse(""))
                        } else {
                            Ok(Json.toJson(false))
                        }
                    }
                case e@JsError(_) => Future.successful(Redirect(routes.BlogController.load()))
            }
        }.getOrElse(Future.successful(Redirect(routes.BlogController.load())))
    }

    def getBlog = Action.async { implicit request =>
        val optionUser = request.session.get("username")
        optionUser.map { username =>
            model.getBlogs.map(blogs => Ok(Json.toJson(blogs.reverse)))
        }.getOrElse(Future.successful(Ok(Json.toJson(Seq.empty[String]))))
    }


    def addPost = Action.async { implicit request =>
        val optionUser = request.session.get("username");
        optionUser.map { username =>
            request.body.asJson.map { body =>
                Json.fromJson[PostData](body) match {
                    case JsSuccess(blog, path) =>
                        model.addBlog(blog.title , username, blog.post).map(count => Ok(Json.toJson(count>0)))
                    case e@JsError(_) => Future.successful(Redirect(routes.BlogController.load()))
                }
            }.getOrElse(Future.successful(Ok(Json.toJson(false))))
        }.getOrElse(Future.successful(Ok(Json.toJson(false))))
    }


    def createUser = Action.async {implicit request =>
        request.body.asJson.map { body =>
            Json.fromJson[UserData](body) match {
                case JsSuccess(ud, path) =>
                    model.createUser(ud.username, ud.password).map { userCreated =>
                        if (userCreated) {
                            Ok(Json.toJson(true))
                              .withSession("username" -> ud.username, "csrfToken" -> play.filters.csrf.CSRF.getToken.map(_.value).getOrElse(""))
                        } else {
                            Ok(Json.toJson(false))
                        }
                    }
                case e @ JsError(_) => Future.successful(Redirect(routes.BlogController.load()))
                }
        }.getOrElse(Future.successful(Redirect(routes.BlogController.load())))
    }

    def logout =  Action { implicit request =>
        Ok(Json.toJson(true)).withSession(request.session - "username")
    }

}