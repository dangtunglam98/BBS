package models
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.{ExecutionContext, Future}
import models.Tables._
class BlogPostInDatabase(db: Database)(implicit ec: ExecutionContext) {
  def createUser(username: String, password: String) : Future[Boolean] = {
    val matches = db.run(Users.filter(userRow => userRow.username === username).result)
    matches.flatMap { userRow =>
      if (userRow.isEmpty) {
        db.run(Users += UsersRow(-1, username, password)).map(addCount => addCount > 0)
      } else {
        Future.successful(false)
      }
    }
  }

  def validateUser(username: String, password: String): Future[Boolean] = {
    val matches = db.run(Users.filter(userRow => userRow.username === username && userRow.password === password).result)
    matches.map(userRow => userRow.nonEmpty)
  }
  def getBlogs : Future[Seq[(String, String, String)]] = {

    db.run(
      (for {
        post <- Posts
      } yield {
        post
      }).result
    ).map(items => items.map(item => (item.posttitle, item.postowner, item.posttext)))
  }

  def addBlog(title: String, username: String, post: String) : Future[Int] = {
    db.run(Posts += PostsRow(-1, title, username, post))

  }
}
